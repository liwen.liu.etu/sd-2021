#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "tableau.h"
#include "error.h"

/**
 * @file
 * @brief Implantation redimensionnable d'un tableau de symboles
 * @author F. Boulier
 * @date mars 2011
 */

/**
 * @brief Constructeur. Initialise \p table. Cette fonction devrait
 * être appelée avant toute autre utilisation de \p table.
 * @param[out] table un tableau de symboles
 */

void init_tableau (struct tableau* table)
{
    table->size = 0;
    table->alloc = 0;
    table->tab = (struct symbole*)0;
}


/**
 * @brief Destructeur. Libère les ressources consommées par \p table.
 * Cette fonction devrait être appelée après la dernière utilisation
 * de \p table.
 * @param[in,out] table un tableau de symboles
 */

void clear_tableau (struct tableau* table)
{   int i;

    if (table->tab != (struct symbole*)0)
    {   for (i = 0; i < table->alloc; i++)
            clear_symbole (&table->tab [i]);
        free (table->tab);
    }
}

static void realloc_tableau (struct tableau* table, int n)
{   struct symbole* newtab;

    if (table->alloc < n)
    {   newtab = (struct symbole*)realloc
                            (table->tab, n * sizeof (struct symbole));
        if (newtab == (struct symbole*)0)
            error ("realloc_tableau", __FILE__, __LINE__);
        table->tab = newtab;
    }
    while (table->alloc < n)
    {   init_symbole (&table->tab [table->alloc]);
        table->alloc += 1;
    }
}

/**
 * @brief Recherche un symbole d'identificateur \p clef dans \p table.
 * S'il existe, affecte son adresse à \p *sym, sinon affecte zéro à \p *sym.
 * @param[out] sym l'adresse d'un symbole
 * @param[in] clef une chaîne de caractères
 * @param[in] table un tableau de symboles
 */

int rechercher_symbole_dans_tableau 
		(struct symbole** sym, char* clef, struct tableau* table)
{   bool found;
    int i, comp, nbcomp;

    nbcomp = 0;
    found = false;
    i = 0;
    while (i < table->size && !found)
    {	nbcomp += 1;
	comp = strcmp (clef, table->tab [i].ident);
	if (comp == 0)
	    found = true;
	else
	    i += 1;
    }
    if (found)
	*sym = &table->tab [i];
    else
	*sym = (struct symbole*)0;
    return nbcomp;
}

/**
 * @brief Ajoute le symbole \p sym dans le tableay \p table
 * @param[in,out] table un tableau de symboles
 * @param[in] sym un symbole
 */

void ajouter_symbole_dans_tableau (struct tableau* table, struct symbole* sym)
{
    if (table->size >= table->alloc)
	realloc_tableau (table, 2 * table->alloc + 1);
    set_symbole (&table->tab [table->size], sym);
    table->size += 1;
}
