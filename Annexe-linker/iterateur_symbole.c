#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include "error.h"
#include "iterateur_symbole.h"

/**
 * @file
 * @brief Implantation des itérateurs de symboles
 * @author F. Boulier
 * @date novembre 2010
 */

/**
 * @brief Constructeur. Initialise l'itérateur \p iter au début de
 * \p filename, qui doit être un fichier objet ou une bibliothèque.
 * Retourne le premier symbole, ou le pointeur nul si le fichier
 * ne comporte pas de symboles. 
 * @param[out] iter un itérateur de symboles
 * @param[in] filename un nom de fichier
 */

struct symbole* first_symbole (struct iterateur_symbole* iter, char* filename)
{   char* cmd [] = { "nm", 
#if defined (NM_MOINS_P)
			   "-p",
#endif
				 (char*)0, (char*)0 };
    pid_t process;
    int fd [2], err;

    iter->filename = (char*)0;
    iter->f = (FILE*)0;
    iter->uniquement_objet = false;
    init_objet (&iter->objet);
    iter->i = -1;

    iter->est_objet = est_fichier_objet (filename);
    if (! iter->est_objet && ! est_fichier_bibliotheque (filename))
	error ("first_symbole", __FILE__, __LINE__);

    cmd [sizeof (cmd) / sizeof (char*) - 2] = filename;

    if (pipe (fd) == -1)
        error ("first_symbole", __FILE__, __LINE__);

    err = open ("/dev/null", O_WRONLY, 0666);
    if (err == -1)
	error ("first_symbole", __FILE__, __LINE__);

    process = fork ();

    if (process == 0)
    {   close (fd [0]);
        dup2 (fd [1], 1);
        close (fd [1]);
	dup2 (err, 2);
	close (err);
        execvp (cmd [0], cmd);
/* this line is never reached */
    }

    close (fd [1]);
    close (err);

    iter->f = fdopen (fd [0], "r");
    if (iter->f == (FILE*)0)
        error ("first_symbole", __FILE__, __LINE__);

    iter->filename = strdup (filename);
    if (iter->filename == (char*)0)
	error ("first_symbole", __FILE__, __LINE__);
    return next_symbole (iter);
}

/**
 * @brief Constructeur. Initialise I sur le premier symbole de l'objet
 * courant de l'itérateur J (qui est censé parcourir une bibliothèque).
 * Seuls les symboles de l'objet courant de J seront parcourus par I.
 * Les données utiles de J sont dupliquées dans I.
 * @param[out] I un symbole
 * @param[in] J un symbole
 */

struct symbole* first_symbole_objet_courant
                (struct iterateur_symbole* I, struct iterateur_symbole* J)
{
#if defined (DEBUG)
    if (J->est_objet)
	error ("first_symbole_objet_courant", __FILE__, __LINE__);
#endif
    I->filename = (char*)0;
    I->f = (FILE*)0;
    I->est_objet = false;
    I->uniquement_objet = true;
    init_objet (&I->objet);
    set_objet (&I->objet, &J->objet);
    I->i = -1;
    return next_symbole (I);
}

/**
 * @brief Destructeur. Cette fonction devrait être appelée après 
 * la dernère utilisation de \p iter, et avant tout autre appel
 * à un constructeur.
 * @param[in,out] iter un itérateur
 */

void clear_iterateur_symbole (struct iterateur_symbole* iter)
{
    if (iter->filename != (char*)0)
	free (iter->filename);
    if (iter->f != (FILE*)0)
	fclose (iter->f);
    clear_objet (&iter->objet);
}

/**
 * @brief Retourne l'adresse du symbole suivant de \p iter, ou le
 * pointeur nul s'il n'y a plus d'autre symbole.
 * @param[in,out] iter un itérateur
 */

struct symbole* next_symbole (struct iterateur_symbole* iter)
{
#define BUFSIZE 256
    static char buffer [BUFSIZE];
    char* fname;
    struct symbole* sym;
    int l;

    if (iter->i + 1 >= iter->objet.size)
    {	if (iter->uniquement_objet)
	    sym = (struct symbole*)0;
	else if (iter->est_objet && iter->i == -1)
	{   charger_objet (&iter->objet, iter->filename, iter->f);
	    iter->i = -1;
	    sym = next_symbole (iter);
	} else
	{   do
	    	fname = fgets (buffer, BUFSIZE-1, iter->f);
	    while (fname != (char*)0 && (l = strlen (fname)) == 1);
	    if (fname == (char*)0)
		sym = (struct symbole*)0;
	    else
	    {	fname [l-1] = '\0';
	    	charger_objet (&iter->objet, fname, iter->f);
	    	iter->i = -1;
	    	sym = next_symbole (iter);
	    }
	}
    } else
    {	iter->i += 1;
	sym = &iter->objet.tab [iter->i];
    }
    return sym;
}

/**
 * @brief Retourne le premier symbole du prochain objet, ou le pointeur
 * nul s'il n'y a plus d'autre symbole. 
 * L'itérateur \p iter est censé parcourir une bibliothèque.
 * @param[in,out] iter un itérateur
 */

struct symbole* next_symbole_next_objet (struct iterateur_symbole* iter)
{
#if defined (DEBUG)
    if (iter->est_objet)
	error ("next_symbole_next_objet", __FILE__, __LINE__);
#endif
    iter->i = iter->objet.size - 1;
    return next_symbole (iter);
}

