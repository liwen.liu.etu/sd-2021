#if ! defined (TABLEAU_H)
#define TABLEAU_H 1

#include "symbole.h"

/**
 * @file
 * @brief Les types nécessaires à une implantation d'un tableau
 * redimensionnable de symboles
 * @author F. Boulier
 * @date mars 2011
 */

/**
 * @struct tableau
 * @brief Le type \p struct \p tableau fournit une implantation très 
 * rudimentaire d'un tableau redimensionnable de symboles
 */

struct tableau 
{   int alloc;           /**< le nombre d'emplacements alloués */
    int size;            /**< le nombre d'emplacements utilisés */
    struct symbole* tab; /**< la zone de stockage */
};

extern void init_tableau (struct tableau*);
extern void clear_tableau (struct tableau*);
extern int rechercher_symbole_dans_tableau 
			(struct symbole**, char*, struct tableau*);
extern void ajouter_symbole_dans_tableau (struct tableau*, struct symbole*);

#endif
