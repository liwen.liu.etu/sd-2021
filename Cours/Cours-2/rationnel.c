/* rationnel.c */

#include <stdio.h>
#include <stdlib.h> // pour malloc et free
#include "rationnel.h"

/* retourne le pgcd de a0 et b0 */

static int Euclide (int a0, int b0)
{   int a, b, t;

    if (a0 > 0) a = a0; else a = - a0;
    if (b0 > 0) b = b0; else b = - b0;
    while (b != 0)
    {   t = a % b;   /* le reste de la division de a par b */
        a = b;
        b = t;
    }
    return a;
}

/*
 * Pas un constructeur !
 * Donc pas de malloc / tab
 * Affecte p0/q0 à R
 * Mériterait de faire partie des fonctions exportées du module
 */

static void set_rationnel (struct rationnel* R, int p0, int q0)
{
    int p, q, g;

    g = Euclide (p0, q0);
    if (q0 > 0)
    {   p = p0 / g;
        q = q0 / g;
    } else
    {   p = - p0 / g;
        q = - q0 / g;
    }
    R->tab[0] = p;
    R->tab[1] = q;
}

/*
 * Constructeur. 
 *     Donc R = struct rationnel passé en mode résultat.
 *     Et Constructeur => R->tab est un pointeur non initialisé
 *     On doit donc faire un malloc.
 */

void init_rationnel (struct rationnel* R, int p0, int q0)
{   
    R->tab = malloc (2 * sizeof (int));
    set_rationnel (R, p0, q0);
}

/*
 * Destructeur.
 *      R = l'adresse d'une var. locale sur le point de disparaître.
 *      C'est le moment de recycler les ressources mobilisées par R.
 *      Donc : free / tab
 */

void clear_rationnel (struct rationnel* R)
{
    free (R->tab);
}

/* Bug ! - corrigé */

/*
 * Ce n'est pas un constructeur (son nom ne commence pas par init_).
 * Donc tous ses paramètres sont initialisés (S inclus)
 * L'appel à init_rationnel sur S qui est initialisé => fuite mémoire
 */

void add_rationnel
    (struct rationnel* S, struct rationnel* A, struct rationnel* B)
{   int p, q;
    p = A->tab[0] * B->tab[1] + A->tab[1] * B->tab[0];
    q = A->tab[1] * B->tab[1];
    set_rationnel (S, p, q);
}

void print_rationnel (struct rationnel R)
{
    if (R.tab[1] == 1)
        printf ("%d\n", R.tab[0]);
    else
        printf ("%d/%d\n", R.tab[0], R.tab[1]);
}

